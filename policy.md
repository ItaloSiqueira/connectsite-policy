
<!DOCTYPE html>
<html>
<head>
<title>Política de Privacidade SAC ConnecteSite</title>
</head>
<body>

<h1>Política de Privacidade do Aplicativo SAC ConnecteSite</h1>
<p>
Última atualização: 13/11/2023 14:50</br>
</br>
<h4>1. Introdução</h4>
Bem-vindo ao SAC ConnecteSite! Esta Política de Privacidade descreve como coletamos, usamos, compartilhamos e protegemos as informações que você nos fornece quando utiliza nosso aplicativo. Sua privacidade é importante para nós, e estamos comprometidos em proteger suas informações pessoais. </br>
</br>
Ao utilizar SAC ConnecteSite, você concorda com os termos e condições desta Política de Privacidade. Se você não concordar com esta política, por favor, não utilize nosso aplicativo.</br>
</br>
<h4>2. Informações que Coletamos</h4>
<h5>2.1 Informações Pessoais</h5>
Coletamos as seguintes informações pessoais quando você utiliza SAC ConnecteSite:</br>
</br>
- Informações de Registro: Quando você realiza o login, coletamos seu login e senha de acesso.</br>
</br>
2.2 Informações do Dispositivo</br>
</br>
Podemos coletar informações sobre o dispositivo que você está usando para acessar o aplicativo, incluindo:</br>
</br>
- Identificadores de Dispositivo: Coletamos identificadores exclusivos do dispositivo, como o ID do dispositivo e informações do sistema operacional.</br>
</br>
3. Como Usamos Suas Informações</br>
</br>
Utilizamos suas informações para os seguintes fins:</br>
</br>
- Prestação de Serviços: Utilizamos suas informações para fornecer os serviços oferecidos pelo aplicativo, como listagem de faturas, listagem de conexões, listagem de contratos e serviços relacionados as informações exibidas.</br>
</br>
- Comunicações: Podemos enviar comunicações relacionadas ao aplicativo, como atualizações, notificações e informações sobre sua conta.</br>
</br>
- Melhorias no Aplicativo: Utilizamos os dados coletados para melhorar o funcionamento e a qualidade do aplicativo.</br>
</br>
- Análise de Dados: Realizamos análises para entender como os usuários interagem com o aplicativo e para melhorar nossos serviços.</br>
</br>
4. Compartilhamento de Informações</br>
</br>
Não compartilhamos suas informações pessoais com terceiros.</br>
</br>
- Cumprimento da Lei: Podemos divulgar informações quando necessário para cumprir com obrigações legais, proteger nossos direitos, segurança ou propriedade.</br>
</br>
5. Segurança das Informações</br>
</br>
Temos medidas de segurança em vigor para proteger suas informações contra acesso não autorizado e uso indevido. No entanto, nenhuma transmissão de dados pela internet ou armazenamento eletrônico é 100% seguro, e não podemos garantir a segurança de suas informações.</br>
</br>
6. Seus Direitos e Escolhas</br>
</br>
Você tem o direito de acessar as informações relacionadas ao seu cadastro. Também pode optar por não receber comunicações promocionais de nossa parte. Para exercer esses direitos, entre em contato conosco.</br>
</br>
7. Menores de Idade</br>
</br>
O aplicativo não é destinado a menores de idade. Se tivermos conhecimento de que coletamos informações de uma criança menor de 13 anos, tomaremos medidas para excluir essas informações.</br>
</br>
8. Alterações na Política de Privacidade</br>
</br>
Reservamo-nos o direito de atualizar esta Política de Privacidade. Quaisquer alterações significativas serão notificadas a você através do aplicativo ou por outros meios.</br>
</br>
9. Contato</br>
</br>
Se tiver alguma dúvida sobre esta Política de Privacidade ou sobre o uso de suas informações pessoais, entre em contato conosco em gerencia@ConnecteSite.com.</br>
</br>
Agradecemos por escolher a ConnecteSite e por confiar em nós com suas informações pessoais. Estamos comprometidos em proteger sua privacidade e proporcionar uma experiência segura e agradável.</p>

</body>
</html>